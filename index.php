<?php 
include "connect.php";
?>
<!DOCTYPE html>
<html>
  
<head><!-- START HEAD -->
  <meta charset="utf-8">
  <!-- START TITLE -->
  <title>AgriFM - Broadcaster</title>
  <!-- END TITLE -->

  <!-- START META, DESCRIPTION, KEYWORDS, AUTHOR -->
  <meta name="description" content="A Template by Carino Technologies" />
  <meta name="keywords" content="HTML, CSS, JavaScript" />
  <meta name="author" content="carinotechnologies" />
  <!-- END META, DESCRIPTION, KEYWORDS, AUTHOR -->

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- START THEME STYLE -->
  <link rel="stylesheet" type="text/css" href="css/normal.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/headhesive.css">
  <link href="css/animate.css" rel="stylesheet">

  <!-- END THEME STYLE -->

  <!-- START FONTELLO ICONS STYLESHEET -->
  <link rel="stylesheet" type="text/css" href="css/fontello.css">
  <link rel="stylesheet" type="text/css" href="css/fontello-ie7.css">
  <!-- END FONTELLO ICONS STYLESHEET -->

  <!-- START MAGNIFIC POPUP STYLESHEET -->
  <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
  <!-- END MAGNIFIC POPUP STYLESHEET -->

  <!-- LOAD GOOGLE FONT OPEN SANS -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700" rel="stylesheet" type="text/css">
  <!-- END GOOGLE FONT OPEN SANS -->


  <!-- START AJAX WEBFONTS -->
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <!-- END AJAX WEBFONTS -->

  <!-- START MOBILE DETECT -->
  <script>if (/mobile/i.test(navigator.userAgent)) document.documentElement.className += ' w-mobile';</script>
  <!-- END MOBILE DETECT -->

  <!-- START FAVICON -->
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <!-- EDN FAVICON -->
    
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script><![endif]-->

</head><!-- END HEAD -->
<body> <!-- START BODY TAG -->
   
  <!-- START HEADER -->
  <!-- SCROLL DIV #boxscroll by nicescroll -->

<div tabindex="1000" style="overflow-y: auto;" id="boxscroll">


<!-- CHANGEABLE UNIQUE SECTOIN ID -->
<section id="home">

  <!-- START FIXED HEADER DIV -->
  <div class="banner">
    <!-- START CONTAINER -->
    <div class="w-container container">
      <div class="w-row">

        <!-- START COLUMN 3 -->
        <div class="w-col w-col-3 logo">
          <!-- START LOGO -->
          <a href="#"><img class="logo" width="100" height="80" src="images/logo2.png" alt="FLAT ASPHALT"></a>
          <!-- END LOGO -->
        </div><!-- END COLUMN 3 -->
      
        <!-- START COLUMN 9 -->
        <div class="w-col w-col-9">

          <!-- START NAVIGATION -->
          <div class="w-nav navbar" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1">
            <div class="w-container nav"><!-- START CONTAINER -->

              <!-- START NAVIGATION LINKS -->
              <nav class="w-nav-menu nav-menu" role="navigation">

                <a class="w-nav-link menu-li" href="#home">Home</a>
                <a class="w-nav-link menu-li" href="index.php#request">Request</a>
                <a class="w-nav-link menu-li" href="index.php#service">Profile</a>
                <a class="w-nav-link menu-li" href="index.php#team">Channel</a>
                <a class="w-nav-link menu-li" href="index.php#portfolio">Gallery</a>
                <a class="w-nav-link menu-li" href="index.php#contact">Contact</a>

              </nav>
              <!-- END NAVIGATION LINKS -->
              
              <!-- START MOBILE NAVIGATION BUTTON * DO NOT DELETE THIS DIV* -->
              <div class="w-nav-button">
                <div class="w-icon-nav-menu"></div>
              </div><!-- END MOBILE NAVIGATION BUTTON -->

            </div>
            <!-- END CONTAINER -->
          </div>
          <!-- END NAVIGATION -->
        </div>
        <!-- END COLUMN 9 -->
      </div>
    </div>
    <!-- END CONTAINER -->
  </div>
 <!-- END FIXED HEADER DIV -->
</section>
<!-- END SECTION -->
<!-- END HEADER -->

<div class="header-parallax" data-stellar-background-ratio="0.5">
<div class="slidersection">

  

<div class="overlay">

  <div class="center fixed-content">
    <div class="center-fix">
        <h1 class="underline animated fadeInDown">AGRIFM<strong class="green">Broadcaster</strong></h1>
        <h4 class="animated fadeInUp delay-05s" style="margin-bottom: 50px;">Science and Music<strong class="green">Station</strong></h4>
        <a class="button animated fadeInUp delay-1s" href="index.html#request" style="margin-right: 20px;"><span><i class="cogelegance-icons-"></i></span>Request</a>
        <a class="button border animated fadeInUp delay-1s" href="index.html#contact">Contact Us</a>
    </div>
  </div>
</div>
</div>
</div>


<!--///////////////////////////////////////////////////////

       // End slider section 

       //////////////////////////////////////////////////////////-->



  <div id="request">

    <div class="row-gree">

      <div class="w-container wrap">
        <div class="center">
          <h1 class="underline">Request Yuk</h1>
        </div>
      </div>

    </div>

    <div class="parallax-back" data-stellar-background-ratio="0.5">
        
        <div class="opcaity">

          <div class="w-container wrap">

              <div class="w-col w-col-6 wp1">
                  
                    <div class="center">

                        <h2 class="underline">Form<span class="green">Request</span></h2>

                          <p><strong>Kirim pesan</strong> dan request lagu disini yuk</p>

                      </div>

                  <div class="w-form">

                    <form action="create_request.php" method="post">

                        <div class="underline animated fadeInDown">

                            <label for="name">Nama Kamu :</label>

                            <input class="w-input" type="text" id="name" placeholder="Enter your name" name="name" required>
                        </div>
                        
                         

                       <div class="underline animated fadeInDown">
                        
                         <div class="w-col-12">

                             <label for="email">Lagu (opsional) :</label>

                             <input class="w-input" placeholder="Enter your song (ex: Pillowtalk)" type="text"  id="email" name="song" required>
                            
                        </div>
                           
                        
                           
                         <div class="w-col-12">

                             <label for="email">Penyanyi (opsional) :</label>

                             <input class="w-input" placeholder="Enter song's singer (ex: Zayn Malik)" type="text"  id="email" name="singer" required>
                            
                        </div>

                       </div>

                       <div class="underline animated fadeInDown">

                           <label for="message">Pesan Kamu :</label>

                             <textarea class="w-input" placeholder="Enter your Message Here" id="message" name="message" required></textarea><br>

                           <input class="button medium" type="submit" value="Send" required>
                       </div>

                    </form>

                      <div class="w-form-done">

                          <p>Thank you! Your submission has been received!</p>

                      </div>

                        <div class="w-form-fail">

                          <p>Oops! Something went wrong while submitting the form :(</p>

                        </div>

                  </div>

                </div> 
            
              
              <div class="w-col w-col-6 wp1">

                      <div class="center">

                        <h2 class="underline">Our Twitter<span class="green">Timeline</span></h2>

                          <p><strong>Selain memakai form</strong> kamu dapat menggunakan twitter</p>

                      </div>

                      <div class="center">

                         <div class="w-col w-col-12 wp2">

                            <a class="twitter-timeline" href="https://twitter.com/aalviian" data-widget-id="728403040571510784">Tweets by @aalviian</a>

                         </div>
                      </div>
                  
              </div>

            </div>
            
        </div>

      </div>

    </div>



<!--///////////////////////////////////////////////////////

       // Service section 

       //////////////////////////////////////////////////////////-->

    
  <div id="service">

    <div class="row-gree">

      <div class="w-container wrap">
        <div class="center">
          <h1 class="underline">Profile</h1>
            <h2><p>
                <a class="w-nav-link menu-li" href="index.php#profile#struktur">Struktur</a>
                <a class="w-nav-link menu-li" href="index.php#profile#visimisi">Visi & Misi</a>
                <a class="w-nav-link menu-li" href="index.php#profile#sejarah">Sejarah</a>
            </p></h2>
        </div>
      </div>

    </div>
      
    <div id="profile#struktur">

    <div class="row-back">

      <div class="w-container wrap-normal">

        <div class="center">

            <h2>Struktur<span class="green">Organisasi</span></h2>

        </div>

          <div class="w-col w-col-12">

                <div class="center w-col w-col-12 wp4">

                    <img src="images/struktur.jpg">

                </div>
 
          </div>

       </div>

     </div>
    
    </div>   
      
<!-- SUB MENU VISI MISI -->
      
    <div id="profile#visimisi">

    <div class="parallax-back3"  data-stellar-background-ratio="0.5">
        <div class="opcaity">

              <div class="w-container wrap-normal">

                <div class="center">

                    <h2><span class="toska">Visi & Misi</span></h2>

                </div>

                  <div class="w-col w-col-12">

                        <div class="center w-col w-col-12 wp4">

                            <h2 class="section-title">
                                Coming Soon
                            </h2><!-- /.Section-title  -->

                            <div id="time_countdown" class="time-count-container">

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash days_dash animated" data-min="0" data-animation="rollIn" data-animation-delay="300">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Days</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash hours_dash animated" data-animation="rollIn" data-animation-delay="600">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>	
                                        </span>
                                        <span class="time-name">Hours</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash minutes_dash animated" data-animation="rollIn" data-animation-delay="900">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Minutes</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash seconds_dash animated" data-animation="rollIn" data-animation-delay="1200">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Seconds</span>
                                    </div>
                                </div>
                            </div>                 

                        </div>

                      </div>

                  </div>

               </div>
        </div>
    </div>
        
    </div>
      
<!-- END VISI MISI -->      
  
<!-- SUB MENU SEJARAH -->   
    
    <div id="profile#sejarah">

    <div class="parallax-back2"  data-stellar-background-ratio="0.5">
        <div class="opcaity">

              <div class="w-container wrap-normal">

                <div class="center">

                    <h2><span class="toska">Sejarah</span></h2>

                </div>

                  <div class="w-col w-col-12">

                        <div class="center w-col w-col-12 wp4">

                            <h2 class="section-title">
                                Coming Soon
                            </h2><!-- /.Section-title  -->

                            <div id="time_countdown2" class="time-count-container">

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash days_dash animated" data-min="0" data-animation="rollIn" data-animation-delay="300">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Days</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash hours_dash animated" data-animation="rollIn" data-animation-delay="600">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>	
                                        </span>
                                        <span class="time-name">Hours</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash minutes_dash animated" data-animation="rollIn" data-animation-delay="900">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Minutes</span>
                                    </div>
                                </div>
                            </div>

                            <div class="w-col w-col-3">
                                <div class="time-box">
                                    <div class="time-box-inner dash seconds_dash animated" data-animation="rollIn" data-animation-delay="1200">
                                        <span class="time-number">
                                            <span class="digit">0</span>
                                            <span class="digit">0</span>
                                        </span>
                                        <span class="time-name">Seconds</span>
                                    </div>
                                </div>
                            </div>                 

                        </div>

                      </div>

                  </div>

               </div>
        </div>
    </div>
        
    </div>
      
<!-- END SEJARAH -->      
    
</div>  
      
</div>



<!--///////////////////////////////////////////////////////

       // End Service section 

       //////////////////////////////////////////////////////////-->

    

<!--///////////////////////////////////////////////////////

       // Team section 

       //////////////////////////////////////////////////////////-->

 <div class="row-gree" id="team">
  <div class="w-container wrap">
    <h1 class="center underline">Channel Programme</h1>
  </div>
 </div>

  <div class="row-back">
      <div class="w-container wrap-normal">
        <div class="center">
          <p>Jadwal program mungkin bisa<strong class="green"> berubah</strong></p>
        </div>
    <!-- Baris ke 1 -->
        <div class="center w-col w-col-3 wp4">
              <div class="team-album">
                <div class="tumbnail">
                  <a class="team-hyper">
                  <img class="thumbnail-img" src="images/team/image1.png" alt="team1">
                  <span>
                    <h3 class="team-heading">Turn Back Time</h3>
                    <p class="team-designation">Penyiar Dapat Berubaherubah</p>
                    <p class="team-des">Turn Back Time adalah program tentang sharing kehidupan mengenai keluarga, persahabatan, asmara dan spiritual.</p>
                  </span>
                </a>
                <div class="team-social">
                    <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
                  </div>
                </div>
              </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-05s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image2.png" alt="team1">
              <span>
                <h3 class="team-heading">Beyond The Sea</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">Beyond The Sea adalah program yang mengundang narasumber mahasiswa IPB yang pernah melakukan perjalanan ke luar negri.</p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-1s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image3.png" alt="team1">
              <span>
                <h3 class="team-heading">Agrisport</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">Agrisport adalah program mengenai berita-berita olahraga yang sedang berlangsung.</p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-1-5s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image4.png" alt="team1">
              <span>
                <h3 class="team-heading">Keliling Indonesia</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">Keliling Indonesia adalah program yang mendatangkan narsumber mahasiswa IPB yang akan berbagi mengenai tempat ia tinggal.</p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>
    <!-- Baris kedua -->
        <div class="center w-col w-col-3 wp4">
              <div class="team-album">
                <div class="tumbnail">
                  <a class="team-hyper">
                  <img class="thumbnail-img" src="images/team/image5.png" alt="team1">
                  <span>
                    <h3 class="team-heading">Mezzoforte</h3>
                    <p class="team-designation">M Irfan Lubis</p>
                    <p class="team-des">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum repellat possimus laboriosam et.</p>
                  </span>
                </a>
                <div class="team-social">
                    <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                    <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
                  </div>
                </div>
              </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-05s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image6.png" alt="team1">
              <span>
                <h3 class="team-heading">My Campus</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">My Campus adalah program tentang apapun yang berkaitan dengan Kampus Institut Pertanian Bogor.</p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-1s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image7.png" alt="team1">
              <span>
                <h3 class="team-heading">REMAJA</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">REMAJA singkatan dari Reequest Malam Aja.Program tempat menitip salam dan reuquest lagu </p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="center w-col w-col-3 wp4 delay-1-5s">
          <div class="team-album">
            <div class="tumbnail">
              <a class="team-hyper">
              <img class="thumbnail-img" src="images/team/image8.png" alt="team1">
              <span>
                <h3 class="team-heading">Green Mind</h3>
                <p class="team-designation">Penyiar Dapat Berubah</p>
                <p class="team-des">Green Mind adalah program mengenai bahasan tentang lingkungan hidup</p>
              </span>
            </a>
            <div class="team-social">
                <a class="social-icons" href="" titlle="Facebook"><i class="facebookelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Twitter"><i class="twitter-birdelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Linkedin"><i class="linkedinelegance-icons-"></i></a>
                <a class="social-icons" href="" titlle="Google+"><i class="googleplus-rectelegance-icons-"></i></a>
              </div>
            </div>
          </div>
        </div>

       


  </div>
</div>


<!--///////////////////////////////////////////////////////

       // End Team section 

       //////////////////////////////////////////////////////////-->



<!--///////////////////////////////////////////////////////

       // Porfolio section 

       //////////////////////////////////////////////////////////-->



  <div id="portfolio">

    <div class="row-gree">

      <div class="w-container wrap">

        <div class="center">

          <h1 class="underline">Gallery</h1>

        </div>

      </div>

    </div>

    <div class="row-back">

      <div class="w-container center">

        <div class="row">

          <div class="options">

            <ul>
                
                        <li class="filter button border medium active" data-filter="all">Show All</li>
	            <?php 
                    $query = mysqli_query($conn, "SELECT * FROM kategori");
                    while ($data = mysqli_fetch_assoc ($query)) { ?>
                        <li class="filter button border medium" data-filter="<?php echo $data['id_kategori']?>"><?php echo $data['kategori']?></li>
                    <?php } 
                ?>         
            </ul>

          </div>

        </div>

        </div>

        <ul class="lb-album" id="Grid">
        <?php 
            $query2 = mysqli_query($conn, "SELECT * FROM gallery");
            while ($data2 = mysqli_fetch_assoc ($query2)) { ?>

            <li class="mix <?php echo $data2['id_kategori']?> mix_all">
             <a class="image-popup-no-margins" alt="responsive" href="admin/<?php echo $data2['foto']?>">
                <img src="admin/<?php echo $data2['foto']?>" width="400" height="300">
                <span>
                  <h4 class="underline heading-description"><?php echo $data2['nama_foto']?></h4>
                  <p class="center description"><?php echo $data2['waktu_upload']?></p>
                </span>
              </a>
            </li>
        <?php } ?>
        </ul>
        <br>
        <br>
        <br>
    </div>

  </div>



<!--///////////////////////////////////////////////////////

       // End Porfolio section 

       //////////////////////////////////////////////////////////-->


<!--///////////////////////////////////////////////////////

       // Contact section 

       //////////////////////////////////////////////////////////-->



  <div id="contact">
      
    <div class="row-gree">

      <div class="w-container wrap">

        <div class="center">

          <h1 class="underline">Contact Us</h1>

        </div>

      </div>

    </div>

    <div class="parallax-back"  data-stellar-background-ratio="0.5">
      <div class="opcaity">
      <div class="w-container wrap">

                  <div class="center">
                    <p>Contact :
                    <br>Radio IPB 107.7 FM |
                    <br>Gd. Agrimedia Elektronik - Green TV IPB | 
                    <br>SMS Center : 0823 1077 0909 | 
                    <br>CP : @irchfan !streaming : http://agrifm.ipb.ac.id:1077 
                    <div class="social-footter">
                      <i class="facebookelegance-icons-"></i>
                      <i class="twitter-birdelegance-icons-"></i>
                      <i class="vimeoelegance-icons-"></i>
                      <i class="friendfeedelegance-icons-"></i>
                      <i class="deviantartelegance-icons-"></i>
                      <i class="lastfmelegance-icons-"></i>
                      <i class="linkedinelegance-icons-"></i>
                      <i class="picasaelegance-icons-"></i>
                      <i class="wordpresselegance-icons-"></i>
                      <i class="instagramelegance-icons-"></i>
                      <i class="tumblrelegance-icons-"></i>
                    </div>

          <div class="w-form">

            <form action="create_feedback.php" method="post">

            <label for="name"></label>

             <input class="w-input" type="text" id="name" placeholder="Enter your name" name="nama">

           <label for="email"></label>

             <input class="w-input" placeholder="Enter your email address" type="text"  id="email" name="email" required="required">

           <label for="message"></label>

             <textarea class="w-input message" placeholder="Enter your Message Here" id="message" name="pesan"></textarea><br>

           <input class="button medium" type="submit" value="Send">

         </form>

          <div class="w-form-done">

              <p>Thank you! Your submission has been received!</p>

            </div>

            <div class="w-form-fail">

              <p>Oops! Something went wrong while submitting the form :(</p>

            </div>

          </div>

        </div>

      </div>
    </div>
    </div>

  </div>

  <div class="w-widget w-widget-map contac-map" data-widget-latlng="-6.559984, 106.724623" data-widget-style="roadmap" data-widget-zoom="25" data-widget-tooltip="AgriFM, IPB"></div>



<!--///////////////////////////////////////////////////////

       // End Contact section 

       //////////////////////////////////////////////////////////-->



<!--///////////////////////////////////////////////////////

       // Footer section 

       //////////////////////////////////////////////////////////-->  



 <div class="footer row-back">
  <div class="w-container wrap-normal center">© <strong>Suka Suka Kita</strong> 2016 - <span class="green">AgriFM</span> Broadcaster </div>
 </div>

       <!--///////////////////////////////////////////////////////

       // End Footer section 

       //////////////////////////////////////////////////////////-->
</div>

<!-- START JQUERY PLUGINS LOAD -->
  <script src="js/jquery.js"></script>
  <script src="js/modernizr.js"></script>
  <script src="js/normal.js"></script>
  <script src="js/carousels.js"></script>
  <script src="js/jquery.stellar.js"></script>
  <script src="js/classie.js"></script>
  <script src="js/jquery.mixitup.js"></script>
  <script src="js/testimonials.js"></script>
  <script src="js/jquery.nicescroll.js"></script>
  <script src="js/rotating-text.js"></script>
  <script src="js/jquery.magnific-popup.js"></script>
  <script src="js/headhesive.min.js"></script>
  <script src="js/waypoints.min.js"></script>
  <script src="js/scripts.js"></script>
  
    <!-- Plugins -->
    <script type="text/javascript" src="js/plugins.js"></script>
    <!-- Custom JavaScript Functions -->
    <script type="text/javascript" src="js/functions.js"></script>
    <script src="js/bootstrap.min.js"></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

  <!-- END JQUERY PLUGINS LOAD -->

</body>

</html>