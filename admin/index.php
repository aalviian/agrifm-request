<?php
	include "header.php";
?>
			<div id="page-wrapper">
				<div class="graphs">
					<div class="col_3">
						<div class="col-md-3 widget widget1">
							<div class="r3_counter_box">
								<i class="fa fa-users"></i>
								<div class="stats">
								  <h5>50 <span>Penyiar</span></h5>
								  <div class="grow grow1">
									<p>Penyiar</p>
								  </div>
								</div>
							</div>
						</div>
						<div class="col-md-3 widget widget1">
							<div class="r3_counter_box">
								<i class="fa fa-pencil-square-o"></i>
								<div class="stats">
								  <h5>45 <span>Request</span></h5>
								  <div class="grow">
									<p>Request</p>
								  </div>
								</div>
							</div>
						</div>
						<div class="col-md-3 widget widget1">
							<div class="r3_counter_box">
								<i class="fa fa-laptop"></i>
								<div class="stats">
								  <h5>10 <span>Program</span></h5>
								  <div class="grow grow3">
									<p>Program</p>
								  </div>
								</div>
							</div>
						 </div>
						 <div class="col-md-3 widget">
							<div class="r3_counter_box">
								<i class="fa fa-bullhorn"></i>
								<div class="stats">
								  <h5>1277 <span>Pendengar</span></h5>
								  <div class="grow grow2">
									<p>Pendengar</p>
								  </div>
								</div>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>

			<!-- switches -->
		<div class="switches">
			<div class="col-12">
				<div class="col-md-12">
                    <canvas id="lineChart"></canvas>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>Team Developer | AgriFM</p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
    <!-- Chart.js -->
<script src="js/Chart.js/dist/Chart.min.js"></script>
         //LINE CHART
          // Line chart
<script>
      var ctx = document.getElementById("lineChart");
      var lineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: "Grafik Request 2016",
            backgroundColor: "rgba(23, 45, 68, 0.37)",
            borderColor: "rgba(23, 45, 68, 1)",
            pointBorderColor: "rgba(23, 45, 68, 1)",
            pointBackgroundColor: "rgba(23, 45, 68, 1)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: [31, 74, 6, 39, 20, 85, 7]
          }]
        },
      });
      //LINE CHART
</script>
</body>
</html>