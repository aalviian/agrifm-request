<?php
	include "header.php";
?>

			<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Request</h3>
					 <div class="xs tabls">
						<div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
							<div class="panel-heading">
								<h2>Daftar Request</h2>
							</div>
                            <br>
                            <br>
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        
                              <thead>
                                <tr>
                                  <th>ID_Request</th>
                                  <th>Nama Pengirim</th>
                                  <th>Lagu</th>
                                  <th>Pesan</th>
                                  <th>Waktu</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php
                              $query = mysqli_query($conn,"SELECT * FROM request ORDER BY waktu desc");
                              while ($data = mysqli_fetch_assoc($query)) {
                              ?>
                                <tr>
                                      <td><?php echo $data['id_request'];?></td>
                                      <td><?php echo $data['nama'];?></td>
                                      <td><?php echo $data['nama_lagu'];?></td>
                                      <td><?php echo $data['pesan'];?></td>
                                      <td><?php echo $data['waktu'];?></td>
                                      <td><a href="prosesapprove.php?id_request=<?php echo $data['id_request'];?>" class="btn <?php if($data['status'] == "terbaca") echo "btn-info"; else echo "btn-default"; ?>" role="button"><?php echo $data['status'];?></a> | <a href="delete_request.php?id_request=<?php echo $data['id_request'];?>" class="btn btn-danger" role="button">Delete</a></td>  
                                </tr>
                              <?php } ?>
                              </tbody>
                            </table>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer section start-->
			<footer>
			  <p>Team Developer | AgriFM</p>
			</footer>
        <!--footer section end-->
	</section>
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="js/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="js/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="js/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>

    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();
          
        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
</body>
</html>