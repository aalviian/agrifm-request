<?php
	include "connect.php";
    if(empty($_SESSION['status']))
    {?>
        <script language="javascript">alert("Forbidden, non admin do not access this page");</script>
        <script type="text/javascript">window.location.assign("login.php")</script>
    <?php
    }

    if($_SESSION['status'] == "admin") {
    $status = $_SESSION['status'];
    $user = $_SESSION['id_penyiar'];
    $query = mysqli_query($conn, "SELECT * FROM penyiar WHERE id_penyiar ='$user'");
    $data = mysqli_fetch_assoc($query);
	}
	
	if(basename($_SERVER['PHP_SELF']) == 'create_penyiar.php' || basename($_SERVER['PHP_SELF']) == 'penyiar.php')
		$penyiar = "class = \"active\"";
	else
		$penyiar = "";
	
	if (basename($_SERVER['PHP_SELF']) == 'index.php')
		$dashboard = "class = \"active\"";
	else
		$dashboard = "";
	
	if (basename($_SERVER['PHP_SELF']) == 'request.php')
		$req = "class = \"active\"";
	else
		$req = "";
	
	if (basename($_SERVER['PHP_SELF']) == 'gallery.php')
		$gallery = "class = \"active\"";
	else
		$gallery = "";
	
	if (basename($_SERVER['PHP_SELF']) == 'feedback.php')
		$feedback = "class = \"active\"";
	else
		$feedback = "";
	
	if (basename($_SERVER['PHP_SELF']) == 'lagu.php')
		$lagu = "class = \"active\"";
	else
		$lagu = "";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Galery | Admin</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Easy Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		 <!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
		<!-- Custom CSS -->
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<!-- Graph CSS -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<!-- jQuery -->
		<!-- lined-icons -->
		<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
		<!-- //lined-icons -->
		<link href="js/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
		<link href="js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
		<link href="js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
		<link href="js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
		<!-- chart -->
		<script src="js/Chart.js"></script>
		<!-- //chart -->
		<!--animate-->
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="js/wow.min.js"></script>
		<script>
			 new WOW().init();
		</script>
		<!--//end-animate-->
		<!----webfonts--->
		<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
		<!---//webfonts---> 
		 <!-- Meters graphs -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<!-- Placed js at the end of the document so the pages load faster -->
	</head>	   
	<body class="sticky-header left-side-collapsed"  onload="initMap()">
		<section>
		<!-- left side start-->
			<div class="left-side sticky-left-side">
				<!--logo and iconic logo start-->
				<div class="logo">
					<h1><a href="index.html">Hello, <span>Admin</span></a></h1>
					<br>
					<img src="<?php echo $data['foto']?>" alt="..." width="80" height="70" class="img-circle profile_img">
				</div>
				<div class="logo-icon text-center">
					<a href="index.html"><i class="lnr lnr-home"></i> </a>
				</div>
				<br>
				<br>
				<br>
				<br>  
				<!--logo and iconic logo end-->
				<div class="left-side-inner">
					<!--sidebar nav start-->
						<ul class="nav nav-pills nav-stacked custom-nav">
							<li <?php echo $dashboard; ?>><a href="index.php"><i class="glyphicon glyphicon-home"></i><span>Dashboard</span></a></li>							
							<li <?php echo $req; ?>><a href="request.php"><i class="glyphicon glyphicon-envelope"></i> <span>Request</span></a></li>							
							<li <?php echo $penyiar; ?>><a href="penyiar.php"><i class="glyphicon glyphicon-user"></i> <span>Penyiar</span></a></li>							
							<li <?php echo $gallery; ?>><a href="gallery.php"><i class="glyphicon glyphicon-picture"></i> <span>Gallery</span></a></li>							
							<li <?php echo $feedback; ?>><a href="feedback.php"><i class="glyphicon glyphicon-comment"></i><span>Feedback</span></a></li>							
							<li <?php echo $lagu; ?>><a href="lagu.php"><i class="glyphicon glyphicon-music"></i> <span>Lagu</span></a></li>
						</ul>
					<!--sidebar nav end-->
				</div>
			</div>
		<!-- left side end-->
		
		<!-- main content start-->
			<div class="main-content main-content3">
				<!-- header-starts -->
				<div class="header-section">
				 
				<!--toggle button start-->
				<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->

				<!--notification menu start -->
				<div class="menu-right">
					<div class="user-panel-top">  	
						<div class="profile_details_left">
							<ul class="nofitications-dropdown">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
									<ul class="dropdown-menu">
										<li>
											<div class="notification_header">
												<h3>You have 3 new messages</h3>
											</div>
										</li>
										<li>
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet</p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li class="odd">
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet </p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li>
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet </p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li>
											<div class="notification_bottom">
												<a href="#">See all messages</a>
											</div> 
										</li>
									</ul>
								</li>
								<li class="login_box" id="loginContainer">
									<div class="search-box">
										<div id="sb-search" class="sb-search">
											<form>
												<input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
												<input class="sb-search-submit" type="submit" value="">
												<span class="sb-icon-search"> </span>
											</form>
										</div>
									</div>
									<!-- search-scripts -->
									<script src="js/classie.js"></script>
									<script src="js/uisearch.js"></script>
									<script>
										new UISearch( document.getElementById( 'sb-search' ) );
									</script>
										<!-- //search-scripts -->
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
									<ul class="dropdown-menu">
										<li>
											<div class="notification_header">
												<h3>You have 3 new notification</h3>
											</div>
										</li>
										<li>
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet</p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li class="odd">
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet </p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li>
											<a href="#">
												<div class="user_img"><img src="images/1.png" alt=""></div>
												<div class="notification_desc">
													<p>Lorem ipsum dolor sit amet </p>
													<p><span>1 hour ago</span></p>
												</div>
												<div class="clearfix"></div>	
											</a>
										</li>
										<li>
											<div class="notification_bottom">
												<a href="#">See all notification</a>
											</div> 
										</li>
									</ul>
								</li>	
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">22</span></a>
									<ul class="dropdown-menu">
										<li>
											<div class="notification_header">
												<h3>You have 8 pending task</h3>
											</div>
										</li>
										<li>
											<a href="#">
												<div class="task-info">
													<span class="task-desc">Database update</span><span class="percentage">40%</span>
													<div class="clearfix"></div>	
												</div>
												<div class="progress progress-striped active">
													<div class="bar yellow" style="width:40%;"></div>
												</div>
											</a>
										</li>
										<li>
											<a href="#">
												<div class="task-info">
													<span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
													<div class="clearfix"></div>	
												</div>
												<div class="progress progress-striped active">
													<div class="bar green" style="width:90%;"></div>
												</div>
											</a>
										</li>
										<li>
											<a href="#">
												<div class="task-info">
													<span class="task-desc">Mobile App</span><span class="percentage">33%</span>
													<div class="clearfix"></div>	
												</div>
												<div class="progress progress-striped active">
													<div class="bar red" style="width: 33%;"></div>
												</div>
											</a>
										</li>
										<li>
											<a href="#">
												<div class="task-info">
													<span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
													<div class="clearfix"></div>	
												</div>
												<div class="progress progress-striped active">
													<div class="bar  blue" style="width: 80%;"></div>
												</div>
											</a>
										</li>
										<li>
											<div class="notification_bottom">
												<a href="#">See all pending task</a>
											</div> 
										</li>
									</ul>
								</li>		   							   		
								<div class="clearfix"></div>	
							</ul>
						</div>
						<div class="profile_details">		
							<ul>
								<li class="dropdown profile_details_drop">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<div class="profile_img">	
											<span style="background:url(<?php echo $data['foto']?>)  no-repeat center"> </span> 
											<div class="user-name">
												<p><?php echo $data['nama_penyiar']; $siapaku = $data['nama_penyiar']; ?><span>Administrator</span></p>
											</div>
											<i class="lnr lnr-chevron-down"></i>
											<i class="lnr lnr-chevron-up"></i>
											<div class="clearfix"></div>	
										</div>	
									</a>
									<ul class="dropdown-menu drp-mnu">
										<li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
										<li> <a href="#"><i class="fa fa-user"></i>Profile</a> </li> 
										<li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
									</ul>
								</li>
								<div class="clearfix"> </div>
							</ul>
						</div>			             	
						<div class="clearfix"></div>
					</div>
				</div>
				<!--notification menu end -->
			</div>
		<!-- //header-ends -->