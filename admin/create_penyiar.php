<?php
	include "header.php";
?>

			<div id="page-wrapper">
				<div class="graphs">
                    <h5>
                        <ol class="breadcrumb">
                          <li><a href="index.php">Dashboard</a></li>
                          <li><a href="penyiar.php">Penyiar</a></li>
                          <li>Create Penyiar</li>
                        </ol>
                    </h5>
					<h3 class="blank1">Form Penyiar</h3>
                      <div class="panel-body panel-body-inputin">
  						<form class="form-horizontal" action="createpenyiarproses.php" name="uploader" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Nama Lengkap</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" name="nama" placeholder="Nama Lengkap">
									</div>
								</div>
 								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Email</label>
									<div class="col-sm-8">
										<input type="email" class="form-control1" name="email" placeholder="Email">
									</div>
								</div>                          
								<div class="form-group">
									<label for="disabledinput" class="col-sm-2 control-label">Tanggal Lahir</label>
									<div class="col-sm-8">
										<input type ="date" class="form-control1" name="tglahir" placeholder="Tanggal lahir">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword" class="col-sm-2 control-label">Fakultas</label>
									<div class="col-sm-4">
                                        <select id="mark" name="fakultas" id="selector1" class="form-control1">
                                          <option value="">--</option>
                                          <option value="g">FMIPA</option>
                                          <option value="h">FMIPET</option>
                                        </select>
                                    </div>
								</div>
								<div class="form-group">
									<label for="inputPassword" class="col-sm-2 control-label">Departemen</label>
									<div class="col-sm-4">
                                        <select id="series" name="departemen" id="selector1" class="form-control1">
                                          <option value="Statistika" class="g">STK</option>
                                          <option value="Geofisika dan Meteorologi" class="g">GFM</option>
                                          <option value="Matematika" class="g">MTK</option>
                                          <option value="Ilmu Komputer" class="g">ILKOM</option>
                                          <option value="Kimia" class="g">KIM</option>
                                          <option value="Biologi" class="h">BIO</option>
                                          <option value="Biokimia" class="h">BIK</option>
                                          <option value="Kimia" class="h">KIM</option>
                                        </select>
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword" class="col-sm-2 control-label">Program</label>
									<div class="col-sm-4">
                                        <select name="program" class="form-control1">
                                          <option value="">--</option>
                                          <?php 
                                            $query = mysqli_query($conn, "SELECT * FROM program");
                                            while($data = mysqli_fetch_assoc($query)) {
                                          ?>
                                          <option value="<?php echo $data['id_program']?>" class="bmw"><?php echo $data['nama_program']?></option>
                                          <?php } ?>
                                        </select>
									</div>
								</div>
                                <div class="form-group">
									<label for="inputPassword" class="col-sm-2 control-label">Unggah Foto</label>
									<div class="col-sm-8">
										<input type="file" class="form-control1" name="foto" placeholder="Upload foto">
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <button type="submit" class="btn-success btn">Create</button>
                                    </div>
                                </div>
                            </form>
                        </div>
				</div>
			</div>
		</div>
		<!--footer section start-->
			<footer>
			   <p>Team Developer | AgriFM</p>
			</footer>
        <!--footer section end-->
	</section>
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/jquery.chained.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script>
     $("#series").chained("#mark"); 
</script>
</body>
</html>